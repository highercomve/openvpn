FROM alpine:3.16

ADD files /

# INSTALL RUNTIME DEPENDENCIES
RUN apk update; apk add --no-cache \
		jq \
		curl \
		openrc \
		iptables \
		ip6tables \
		openvpn \
		bash \
		rsyslog; \
	rc-update add zeronetworking sysinit default; \
	rc-update add rsyslog sysinit default; \
	rc-update add ntpd default; \
	rc-update add openvpn-client default; \
	echo root:pantacor | chpasswd


RUN wget \
	https://raw.githubusercontent.com/ProtonVPN/scripts/master/update-resolv-conf.sh \
	-O /etc/openvpn/update-resolv-conf

CMD [ "/sbin/init" ]
