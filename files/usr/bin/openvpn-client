#!/bin/bash

key=/pantavisor/user-meta/openvpn.enabled
json=/vpn/config.json
config=/etc/openvpn/config.ovpn
auth=/etc/openvpn/auth.conf

enabled=false
default_failure_sleep=5
failure_sleep=5
big_failure_sleep_initial=60
big_failure_sleep=$big_failure_sleep_initial
big_failure_sleep_increment=60
big_failure_sleep_max=3600
max_failing=2

phips_fallback=`getent ahostsv4 api.pantahub.com | awk '{ print $1 }'`
echo "Fallback Pantahub IPs: $phips_fallback"

function auth_json_to_file() {
  rawauth=$(cat $1 | jq -r ".auth")
  if ! [ "$rawauth" == "null" -o "$rawauth" == "" ]; then
    cat $1 | jq -r ".auth" > $2
  fi
}

function map_json_to_config() {
  rawconfig=$(cat $1 | jq -r ".config")
  if ! [ "$rawconfig" == "null" -o "$rawconfig" == "" ]; then
    echo -e "script-security 2 \nup /etc/openvpn/up.sh \ndown /etc/openvpn/down.sh \n" > $2
    cat $1 | jq -r ".config" >> $2
  fi
}

function pantahub_host () {
  if [ -f "/pantavisor/pantahub-host" ]; then
    cat /pantavisor/pantahub-host
  else
    echo "https://api.pantahub.com"
  fi
}

ping_ph() {
  # are we reaching a real pantahub instance through
  curl -si `pantahub_host`/auth/auth_status | grep -q "pantahub services"
  return $?
}

vpnup() {
  cp -f /etc/resolv.conf /etc/resolv.conf.wgbackup
  /etc/init.d/openvpn-service start
  return $?
}

vpndown() {
  /etc/init.d/openvpn-service stop
  r=$?
  if [ -f /etc/resolv.conf.wgbackup ]; then
    cat /etc/resolv.conf.wgbackup > /etc/resolv.conf
    rm -f /etc/resolv.conf.wgbackup
  fi
  return $r
}

# in cloudflare
echo 1 > /proc/sys/net/ipv4/ip_forward || true

while true; do
    ping -c 1 -W 1 1.1.1.1 &> /dev/null
    if [ "$?" == "0" ]; then
      break
    fi
done

if ! [ -d /dev/net ];then
 mkdir /dev/net
fi

if ! [ -c /dev/net/tun ]; then
 mknod /dev/net/tun c 10 200
 chmod 666 /dev/net/tun
fi

while true
do
  # no config, lets restart after some sleeping
  if ! [ -f $json ]; then
    $enabled && vpndown &> /dev/null && echo "No configuration file found on: $json"
    enabled=false
    sleep $failure_sleep
    continue
  fi

  # When key file doesn't exist disable vpn
  if [ ! -f $key ]; then
    $enabled && vpndown
    enabled=false
    sleep 1
    continue
  fi

  # When key file exist and connect is not enabled disable vpn
  if [ "$(cat $key)" != "true" ]; then
    $enabled && vpndown &> /dev/null && echo "openvpn has been disable by the user"
    enabled=false
    sleep 1
    continue
  fi

  if ! [ -f $config ]; then
    echo "Converting config.json to openvpn configuration"
    map_json_to_config $json $config
  fi
  
  if ! [ -f $auth ]; then
    echo "Converting config.json to openvpn auth-user-pass file"
    auth_json_to_file $json $auth
  fi 

  # Start vpn connection process
  fail=1
  while ! $enabled; do
    if [ "$fail" -gt "$max_failing" ]; then
      break
    fi

    echo "Connecting to VPN... (try $fail)"
    vpndown &> /dev/null

    vpnup &> /dev/null
    if [ "$?" != "0" ]; then
      fail=$(($fail + 1));
      sleep $failure_sleep 
      continue
    fi

    ping -c 1 -W 1 1.1.1.1 &> /dev/null
    if [ "$?" != "0" ]; then
      fail=$(($fail + 1));
      vpndown &> /dev/null
      sleep $failure_sleep
      continue
    fi

    if ! ping_ph; then
      echo "VPN works, but cannot reach pantahub, trying to setup direct routes"
      gwip=$(ip route | grep ^default | awk '{ print $3 }')
      phips=`getent ahostsv4 api.pantahub.com | awk '{ print $1 }'`
      echo "${phips:-$phips_fallback}" | while read ip; do ip route add $ip/32 via $gwip; done

      sleep 1
      
      if ! ping_ph; then
        echo "${phips:-$phips_fallback}" | while read ip; do ip route del $ip/32 via $gwip; done
        fail=$(($fail + 1));
        vpndown &> /dev/null
        sleep $failure_sleep
        continue
      fi
    fi

    enabled=true
    echo "Connected to VPN on the $fail try"
    echo "Setting Pantahub save routes"
    break
  done

  if [ "$fail" -gt "$max_failing" ]; then
    echo "We lost internet access"
    vpndown &> /dev/null
    failure_sleep=$big_failure_sleep
    echo "After $fail try, openvpn is going to sleep for $failure_sleep"
    if [ $big_failure_sleep -lt $big_failure_sleep_initial ]; then
      big_failure_sleep=$(( $big_failure_sleep + big_failure_sleep_increment ))
    fi
    sleep $failure_sleep
  else
    big_failure_sleep=big_failure_sleep_initial
    failure_sleep=default_failure_sleep
  fi

  ping -c 1 -W 1 1.1.1.1 &> /dev/null
  if [ "$?" != "0" ]; then enabled=false; fi
  
  if [ $enabled == false ]; then echo "openvpn connection lost, reconnecting..."; fi

  sleep 1
done
